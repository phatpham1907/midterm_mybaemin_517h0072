import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/screens/cart_screen.dart';
import 'package:shop_app/screens/product_overview_screen.dart';
import 'package:shop_app/screens/profile_screen.dart';
import 'package:shop_app/screens/user_product_screen.dart';
import 'package:shop_app/widgets/badge.dart';

import '../providers/cart.dart';

class MainScreen extends StatefulWidget {
  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var _selected = 0;

  void _onNavBarTapped(int index) {
    setState(() {
      _selected = index;
    });
  }

  Widget _getPages() {
    switch (_selected) {
      case 0:
        return ProductOverviewScreen();
      case 1:
        return UserProductScreen();
      case 2:
        return ProfileScreen();
      default:
        return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _getPages(),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onNavBarTapped,
        currentIndex: _selected,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.grey,
        backgroundColor: Colors.white,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.shopify),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.badge),
            label: 'Product',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: "Profile",
          )
        ],
      ),
    );
  }
}
