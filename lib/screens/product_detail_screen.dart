import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/helpers/helper.dart';
import 'package:shop_app/providers/products.dart';

class ProductDetailScreen extends StatelessWidget {
  static const routeName = "/product-detail";

  @override
  Widget build(BuildContext context) {
    final productId = ModalRoute.of(context)!.settings.arguments as String;
    final loadedProduct = Provider.of<Products>(
      context,
      listen:
          false, //just need to get data 1 time when loaded, and no possible for changing
      // when you are still on this fucking screen => change to false.
    ).findById(productId);
    // Provider.of<Products>(context).items.firstWhere((prod) => prod.id == productId);
    // move the above line to separate logic with widget

    const defaultPadding = 16.0;
    const defaultBorderRadius = 12.0;

    return Scaffold(
      backgroundColor: const Color(0xFFFBFBFD),
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            expandedHeight: 300,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(loadedProduct.title),
              background: Hero(
                tag: loadedProduct.id,
                child: Image.network(
                  loadedProduct.imageUrl,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate([
            Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text(
                      loadedProduct.title,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                ),
                const SizedBox(width: defaultPadding),
                Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Text(
                    loadedProduct.price.toString()+"\$",
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  vertical: defaultPadding, horizontal: 12),
              child: Text(loadedProduct.description),
            ),
            const SizedBox(height: defaultPadding * 2),
          ]))
        ],
      ),
    );
  }
}
