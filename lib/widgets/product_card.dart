import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/auth.dart';
import '../providers/product.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({
    Key? key,
    required this.imageUrl,
    required this.title,
    required this.price,
    required this.bgColor,
  }) : super(key: key);
  final String imageUrl, title;
  final int price;
  final Color bgColor;

  @override
  Widget build(BuildContext context) {
    const defaultPadding = 16.0;
    const defaultBorderRadius = 12.0;
    final authData = Provider.of<Auth>(context, listen: false);

    return Container(
      // width: 150,
      padding: const EdgeInsets.all(defaultPadding / 2),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(defaultBorderRadius)),
        boxShadow: [BoxShadow(color: Colors.black)],
      ),
      child: Column(
        children: [
          Container(
            height: 140,
            width: double.infinity,
            decoration: BoxDecoration(
              color: bgColor,
              borderRadius:
                  const BorderRadius.all(Radius.circular(defaultBorderRadius)),
            ),
            child: ClipRRect(
              borderRadius:
                  BorderRadius.all(Radius.circular(defaultBorderRadius)),
              child: FadeInImage(
                placeholder:
                    AssetImage('assets/images/product-placeholder.png'),
                image: NetworkImage(imageUrl),
                fit: BoxFit.cover,
              ),
            ),
          ),
          const SizedBox(height: defaultPadding / 1.9),
          Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(color: Colors.black),
                ),
              ),
              const SizedBox(width: defaultPadding / 4),
              Text(
                price.toString() + "\$",
                style: Theme.of(context).textTheme.subtitle2,
              ),
            ],
          ),
          const SizedBox(height: defaultPadding / 1.9),
        ],
      ),
    );
  }
}
